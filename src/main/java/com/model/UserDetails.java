package com.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserDetails {

	private String name;
	private String email;
	private List<String> listOfProjectsAssigned=new ArrayList<String>(Arrays.asList("SutiHR","DAnalytics","Desk"));
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getListOfProjectsAssigned() {
		return listOfProjectsAssigned;
	}
	public void setListOfProjectsAssigned(List<String> listOfProjectsAssigned) {
		this.listOfProjectsAssigned = listOfProjectsAssigned;
	}
}
