package com.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "employees")
@XmlType(propOrder= {"shiftId","shiftName","empoloyeeName","listOfShiftWithEmployees","listOfPersons"})
public class ShiftDetails {

	private String shiftName="Morning Shift";
	private int shiftId=1;
	private String empoloyeeName="Pratyush";
	private List<String> listOfEmployees=new ArrayList<String>(Arrays.asList("SutiHR","DAnalytics","Desk"));
	private Map<Integer,String> listOfShiftWithEmployees=new HashMap<>();
	private List<Person> listOfPersons=new ArrayList<>();
	
	
	public ShiftDetails() {
		super();
	}
	public String getShiftName() {
		return shiftName;
	}
	public void setShiftName(String shiftName) {
		this.shiftName = shiftName;
	}
	public int getShiftId() {
		return shiftId;
	}
	public void setShiftId(int shiftId) {
		this.shiftId = shiftId;
	}
	public String getEmpoloyeeName() {
		return empoloyeeName;
	}
	public void setEmpoloyeeName(String empoloyeeName) {
		this.empoloyeeName = empoloyeeName;
	}
	@XmlTransient
	public List<String> getListOfEmployees() {
		return listOfEmployees;
	}
	public void setListOfEmployees(List<String> listOfEmployees) {
		this.listOfEmployees = listOfEmployees;
	}
	public Map<Integer,String> getListOfShiftWithEmployees() {
		return listOfShiftWithEmployees;
	}
	public void setListOfShiftWithEmployees(Map<Integer,String> listOfShiftWithEmployees) {
		this.listOfShiftWithEmployees = listOfShiftWithEmployees;
	}
	public List<Person> getListOfPersons() {
		return listOfPersons;
	}
	public void setListOfPersons(List<Person> listOfPersons) {
		this.listOfPersons = listOfPersons;
	}
}
