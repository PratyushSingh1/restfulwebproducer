package com.serviceProducer;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class InternalRestCall {

	public static void main(String[] args) {
		
		Client client=new Client();
		
		WebResource resource=client.resource("http://localhost:8080/restfulWebProducer/backend/user/service/processInput");
				 
		ClientResponse clientResponse=resource.accept("text/plain")
				.entity("restCall","text/plain")
				.post(ClientResponse.class);
		
		if(clientResponse.getStatus()==200) {
			String output=clientResponse.getEntity(String.class);
			System.out.println(output);
		}else {
			System.out.println("ERROR IN RESPONSE");
		}
	}
}
