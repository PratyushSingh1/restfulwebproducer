package com.serviceProducer;

import java.awt.PageAttributes.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.codehaus.jackson.annotate.JsonAnySetter;

import com.model.Person;
import com.model.ShiftDetails;
import com.model.UserDetails;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/user/service")
/*
 * class called as resource class
 */
public class UserServices {

	@PUT
	@Path("/create")
	/*
	 * these methods are called as resource methods
	 */
	public void createUser() {
	
		System.out.println("inside createUser method");
	}
	@GET
	@Path("/get/{userid}")
	public void getUser(@PathParam("userid")String userId) {
		System.out.println("inside getUser method");
		System.out.println("parameter passed is ="+userId);
	}
	@POST
	@Path("/update/{userid}")
	public void updateUser(@PathParam("userid")String userId,
			@QueryParam("number")String number,
			@HeaderParam("Host")String headerParam,
			@FormParam("userName")String userName,@FormParam("userEmail")String userEmail) {
		 
		System.out.println("inside updateUser method NAME="+userId+" NUMBER="+number+
				" From="+headerParam);
		System.out.println("---FORM PARAMETERS-----");
		System.out.println("userName="+userName);
		System.out.println("userEmail="+userEmail);		
	}
	@DELETE
	@Path("/delete")
	public void deleteUser() {
		
	}
	
	public SpecialUser getSpecialUser() {
		return new SpecialUser();
	}
	
	@POST
	@Path("/getURIParams/{uriparam1}/{uriparam2}")
	public void getURIParams(@Context UriInfo info) {
		System.out.println("----------QUERY PARAMS--------------");
		MultivaluedMap<String, String> uriMultiMap1=info.getQueryParameters();
		extractKeyValueFromMap(uriMultiMap1);
		System.out.println("----------PATH PARAMS--------------");
		MultivaluedMap<String, String> uriMultiMap2=info.getPathParameters();
		extractKeyValueFromMap(uriMultiMap2);
	}

	@POST
	@Path("/getHeaders")
	public void getHeaderParams(@Context HttpHeaders headers) {
		System.out.println("----------GET HEADER--------------");
		MultivaluedMap<String, String> headerParams=headers.getRequestHeaders();
		extractKeyValueFromMap(headerParams);
	}

	@POST
	@Path("/submitFormParam")
	public void getMultipleFormParameter(MultivaluedMap<String, String> fullFormParam) {
		extractKeyValueFromMap(fullFormParam);
	}
	
	@GET
	@Path("/getResponse/{pathParam}")
	@Produces("text/plain")
	public String getStringResponseAsPlaintext(@PathParam("pathParam") String pathParam,
												@QueryParam("queryParam") String queryparam) {
		return queryparam;
	}
	
	// either we can make our method to produce multiple formats or 
	//we can create seperate methods to produce it then jersey will decide
	//which method to choose based on @Produces annotation
	/*
	 * @GET
	 * 
	 * @Path("/getResponseAsXML")
	 * 
	 * @Produces({"application/json","application/xml"}) public UserDetails
	 * getResponseAsXML(@QueryParam("name") String name,
	 * 
	 * @QueryParam("email") String email) { UserDetails userDetails=new
	 * UserDetails(); userDetails.setEmail(email); userDetails.setName(name); return
	 * userDetails; }
	 */
	
	@GET
	@Path("/getResponseAsXML")
	@Produces({"application/xml" })
	public UserDetails getResponseAsXML(@QueryParam("name") String name, @QueryParam("email") String email) {
		UserDetails userDetails = new UserDetails();
		userDetails.setEmail(email);
		userDetails.setName(name);
		return userDetails;
	}	

	
	@GET
	@Path("/getResponseForShiftDetail")
	@Produces({"application/json"})
	public ShiftDetails getResponse() {
		Map<Integer,String> mapOfShiftWithEmployees=new HashMap<>();
		List<Person> listOfPerson=new ArrayList<>();
		mapOfShiftWithEmployees.put(1,"Hello");
		ShiftDetails details=new ShiftDetails();
		details.setListOfShiftWithEmployees(mapOfShiftWithEmployees);
		
		Person person1=new Person();
		person1.setId(1);
		person1.setName("Pratyush");

		Person person2=new Person();
		person2.setId(2);
		person2.setName("Ramesh");
		listOfPerson.add(person1);
		listOfPerson.add(person2);
		details.setListOfPersons(listOfPerson);
		return details;
	}
	
	@GET
	@Path("/getResponseFromDto")
	@Produces({"application/json"})
	public UserDetails getResponseFromDto(@QueryParam("name") String name, @QueryParam("email") String email) {
		UserDetails userDetails = new UserDetails();
		userDetails.setEmail(email);
		userDetails.setName(name);
		return userDetails;
	}	
	
	
	@POST
	@Path("/processInput")
	@Consumes("application/xml")
	@Produces("text/plain")
	public String processInput(String input) {
		return input;
	}
	@POST
	@Path("/uploadFile")
	@Consumes("multipart/form-data")
	@Produces("text/plain")
	public Response uploadFile(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition attachmentInfo) {
		System.out.println("Inside file upload");
		String uploadFile="/home/pratyush/eclipse-workspace/restfulWebProducer/"+attachmentInfo.getFileName();
		try {
			FileOutputStream outputStream=new FileOutputStream(uploadFile);
			byte[] bytes=new byte[1024];
			int bytesRead=0;
			while((bytesRead=is.read(bytes))!=-1) {
				outputStream.write(bytes,0,bytesRead);
			}
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		File pathOfUploadedfile=new File(uploadFile);
		return Response
				.ok()
				.entity(pathOfUploadedfile)
				.header("Content-Disposition","attachment;filename=\"uploadedFile.pdf\"")
				.build();
	}
	
	@POST
	@Path("/receiveUser")
	@Consumes("text/plain")
	public Response receiveUser(UserDetails details) {
		System.out.println(details.getName());
		System.out.println(details.getEmail());
		return Response.ok().build();
	}
	
	private void extractKeyValueFromMap(MultivaluedMap<String, String> uriMultiMap) {
		
		Set<String> setOfKeys=uriMultiMap.keySet();
		for(String key:setOfKeys) {
			System.out.print("key is "+key+" and value is "+uriMultiMap.getFirst(key));
		}
	}
}
