package com.serviceProducer;

import java.io.*;
import java.net.*;

public class WebsocketTest {

	public static void main(String[] args) {
		HttpURLConnection connection = null;
		String urlParameters="Hello";
		  try {
		    //Create connection
		    URL url = new URL("http://localhost:8080/restfulWebProducer/backend/user/service/processInput");
		    connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("POST");
		    connection.setRequestProperty("Content-Type", "text/plain");

		    connection.setRequestProperty("Content-Length", 
		        Integer.toString(urlParameters.getBytes().length));
		   // connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		    connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		    connection.setUseCaches(false);
		    connection.setDoOutput(true);

		    //Send request
		    DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
		    wr.writeBytes(urlParameters);
		    wr.close();

		    //Get Response  
		    InputStream is = connection.getInputStream();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		    StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
		    String line;
		    while ((line = rd.readLine()) != null) {
		      response.append(line);
		      response.append('\r');
		    }
		    rd.close();
		   System.out.println(response.toString());
		  } catch (Exception e) {
		    e.printStackTrace();
		  //  System.out.println(response.toString());
		  } finally {
		    if (connection != null) {
		      connection.disconnect();
		    }
		  }
	}
}
