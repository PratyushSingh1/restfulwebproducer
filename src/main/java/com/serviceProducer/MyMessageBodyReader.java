package com.serviceProducer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;

import com.model.UserDetails;

public class MyMessageBodyReader implements MessageBodyReader<UserDetails> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public UserDetails readFrom(Class<UserDetails> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
			byte[] bytes=new byte[1024];
			int bytesRead=entityStream.read(bytes);
			String input=new String(bytes,0,bytesRead);
			String[] strArray=input.split("&");
			UserDetails details=new UserDetails();
			details.setName(strArray[0]);
			details.setEmail(strArray[1]);
			return details;
	}
}
