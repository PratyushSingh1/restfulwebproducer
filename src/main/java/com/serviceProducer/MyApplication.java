package com.serviceProducer;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.model.UserDetails;

public class MyApplication extends Application {

	public Set<Class<?>> getClasses(){
		Set<Class<?>> s=new HashSet<Class<?>>();
		s.add(UserServices.class);
		s.add(MyMessageBodyReader.class);
		return s;
	}
}
